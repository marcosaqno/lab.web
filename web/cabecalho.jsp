<%-- 
    Document   : cabecalho
    Author     : Marcos
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <style>
        .container-login {
            border: solid 4px;
            padding-top: 40px;
            padding-bottom: 40px;
            padding-right: 30px;
            padding-left: 30px;
        }
        
        .container-rodape {
            border: solid 4px;
            border-top: 0px;
            padding-top: 40px;
            padding-bottom: 40px;
            padding-right: 30px;
            padding-left: 30px;
        }
        
        .container-menu {
            border: solid 4px;
            padding-top: 40px;
            padding-bottom: 40px;
            padding-right: 30px;
            padding-left: 30px;
        }
        
        .btn-color{
            margin-top: 20px;
            background-color: #FFFAFA;
            color: #000000;
        }
        
        .btn-color:hover{
            background-color: #8B795E;
            color: #FFFAFA;
        }
        
        .bg-3 {
            background-color: #EEE9E9;
            color: #000000;
        }
        
        .bg-1 {
            background-color: #8B8989;
            color: #000000;
        }
        
        p{
            font-size: 24px;
        }
        
        .anuncio { font-family: helvetica, arial, sans-serif;
                font-weight: bold;
                font-size: 2em; 
        }
        
        .anuncio-img {
            height: 50%;
            width: 20%;
        }
        
        </style>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
