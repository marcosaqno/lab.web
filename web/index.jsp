<%@ include file="cabecalho.jsp"%>

<title>Lab.Web</title>

</head>

<body>

    <%@ page session = "false" %>

    <div class="container container-login bg-1">
        <div class="text-center col-lg-4 col-lg-offset-4">

            <% // inicia scriptlet                                                                                     

                String erro = (String) request.getAttribute("erro_login");

                if (erro != null) {
            %>
            <h1><%=erro%></h1>
            <% }

            %>


            <form action="Controller" method="post">

                <h1>Lab. Web</h1>

                <div class="form-group">
                    <label>Login: </label>
                    <input class="form-control" type="text" name="usuario" placeholder="Insira seu nome do usu�rio" />
                </div>

                <div class="form-group">
                    <label>Senha: </label>
                    <input class="form-control" type="password" name="senha" placeholder="Insira a senha: " />
                </div>

                <button class="btn btn-lg btn-color" type="submit" name="codigo_op" value="-1">Entrar</button>

            </form>
        </div>
    </div>
<%@ include file="rodape.jsp"%>

